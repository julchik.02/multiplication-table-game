using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMover : MonoBehaviour
{
    [SerializeField] float _moveSpeed = 5f;
    [SerializeField] GameObject _fireworks;
    void Update()
    {
        var _move = Vector2.zero;
        if (Input.GetKey(KeyCode.RightArrow)) _move.x = 1;
        if (Input.GetKey(KeyCode.LeftArrow)) _move.x = -1;
        if (Input.GetKey(KeyCode.DownArrow)) _move.y = -1;
        if (Input.GetKey(KeyCode.UpArrow)) _move.y = 1;
        transform.Translate(_move * _moveSpeed * Time.deltaTime);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Finish"))
        {
            _fireworks.SetActive(true);
            _moveSpeed = 0;

        }
    }
    public void LoadFirstScene()
    {
        SceneManager.LoadScene(0);
    }
}
