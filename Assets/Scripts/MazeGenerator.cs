using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MazeGeneratorCell
{
    public int X;
    public int Y;
    public bool WallLeft = true;
    public bool WallBottom = true;
    public bool Checked = false;
    public int StartDistance;

}
public class MazeGenerator
{
    public int width = 23;
    public int height = 15;
    public MazeGeneratorCell FinishPosition;
    public MazeGeneratorCell[,] GenerateMaze()
    {
        MazeGeneratorCell[,] _maze = new MazeGeneratorCell[width, height];

        for (int x = 0; x < _maze.GetLength(0); x++)
        {
            for (int y = 0; y < _maze.GetLength(1); y++)
            {
                _maze[x, y] = new MazeGeneratorCell { X = x, Y = y };
            }
        }
        for (int x = 0; x < _maze.GetLength(0); x++)
        {
            _maze[x, height - 1].WallLeft = false;
        }
        for (int y = 0; y < _maze.GetLength(1); y++)
        {
            _maze[width - 1, y].WallBottom = false;
        }

        RemoveWallWithBackTracker(_maze);
        PlaceMazeExit(_maze);
        return _maze;
    }


    private void RemoveWallWithBackTracker(MazeGeneratorCell[,] maze)
    {
        MazeGeneratorCell _current = maze[0, 0];
        _current.Checked = true;
        _current.StartDistance = 0;

        Stack<MazeGeneratorCell> stack = new Stack<MazeGeneratorCell>();
        do
        {
            List<MazeGeneratorCell> _uncheckedNeighbours = new List<MazeGeneratorCell>();
            int x = _current.X;
            int y = _current.Y;
            if (x > 0 && !maze[x - 1, y].Checked) _uncheckedNeighbours.Add(maze[x - 1, y]);
            if (y > 0 && !maze[x, y - 1].Checked) _uncheckedNeighbours.Add(maze[x, y - 1]);
            if (x < width - 2 && !maze[x + 1, y].Checked) _uncheckedNeighbours.Add(maze[x + 1, y]);
            if (y < height - 2 && !maze[x, y + 1].Checked) _uncheckedNeighbours.Add(maze[x, y + 1]);

            if (_uncheckedNeighbours.Count > 0)
            {
                MazeGeneratorCell _chosen = _uncheckedNeighbours[Random.Range(0, _uncheckedNeighbours.Count)];
                RemoveWall(_current, _chosen);
                _chosen.Checked = true;
                stack.Push(_chosen);
                _current = _chosen;
                _chosen.StartDistance = stack.Count;
            }
            else
            { _current = stack.Pop(); }

        } while (stack.Count > 0);
    }

    private void RemoveWall(MazeGeneratorCell a, MazeGeneratorCell b)
    {
        if (a.X == b.X)
        {
            if (a.Y > b.Y) a.WallBottom = false;
            else b.WallBottom = false;
        }
        else
        {
            if (a.X > b.X) a.WallLeft = false;
            else b.WallLeft = false;
        }
    }

    private void PlaceMazeExit(MazeGeneratorCell[,] maze)
    {
        MazeGeneratorCell _furthest = maze[0, 0];
        for (int x = 0; x < maze.GetLength(0); x++)
        {
            if (maze[x, height - 2].StartDistance > _furthest.StartDistance) _furthest = maze[x, height - 2];
            if (maze[x, 0].StartDistance > _furthest.StartDistance) _furthest = maze[x, 0];

        }
        for (int y = 0; y < maze.GetLength(1); y++)
        {
            if (maze[width - 2, y].StartDistance > _furthest.StartDistance) _furthest = maze[width - 2, y];
            if (maze[0, y].StartDistance > _furthest.StartDistance) _furthest = maze[0, y];

        }

    }
}
