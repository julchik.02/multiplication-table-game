using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeSpawner : MonoBehaviour
{
    public GameObject CellPrefab;
    public GameObject[] Background;
    void Start()
    {
        Instantiate(Background[Random.Range(0, Background.Length)]);
        MazeGenerator _generator = new MazeGenerator();
        MazeGeneratorCell[,] _maze = _generator.GenerateMaze();
        for (int x = 0; x < _maze.GetLength(0); x++)
        {
            for (int y = 0; y < _maze.GetLength(1); y++)
            {
                Cell _c = Instantiate(CellPrefab, new Vector2(x, y), Quaternion.identity, this.transform).GetComponent<Cell>();

                _c.WallLeft.SetActive(_maze[x, y].WallLeft);
                _c.WallBottom.SetActive(_maze[x, y].WallBottom);
                if (x == 22 && y == 10) _c.WallLeft.SetActive(false);
                if (x == 0 && y == 0) _c.WallLeft.SetActive(false);
            }
        }
    }
}
