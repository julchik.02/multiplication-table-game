using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    [SerializeField] GameObject _quiz;
    [SerializeField] EndScreen _endScreen;
    [SerializeField] StartScreen _startScreen;

    int _index = 20;
    void Start()
    {
        _quiz.gameObject.SetActive(false);

        _endScreen.gameObject.SetActive(false);
        _startScreen.gameObject.SetActive(true);

    }

    void Update()
    {

        if (_quiz.GetComponent<Quiz>().isLoaded)
        {
            QuizLoading();
            EndScreenOn();
        }

    }
    public void SetButtonIndex(int _buttonIndex)
    {
        _index = _buttonIndex;
    }

    void QuizLoading()
    {
        _startScreen.gameObject.SetActive(false);
        _quiz.gameObject.SetActive(true);

    }

    private void EndScreenOn()
    {
        if (_quiz.GetComponent<Quiz>().isComplete)
        {
            _quiz.gameObject.SetActive(false);
            _endScreen.gameObject.SetActive(true);
        }
    }

    public void OnReplay()
    {
        SceneManager.LoadScene(0);
    }
    public void PlayGame()
    {
        SceneManager.LoadScene(Random.Range(1, 4));
    }
}
