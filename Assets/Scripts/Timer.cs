using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    [SerializeField] float _timeToCompleteQuestion = 30f;
    [SerializeField] float _timeToShowCorrectAnswer = 2f;
    public bool LoadNextQuestion;
    public float FillFraction;
    public bool IsAnsweringQuestion;
    float _timeValue;
    void Update()
    {
        UpdateTimer();
    }
    public void CancelTimer()
    {
        _timeValue = 0;
    }
    void UpdateTimer()
    {
        _timeValue -= Time.deltaTime;
        if (IsAnsweringQuestion)
        {
            if (_timeValue > 0)
            {
                FillFraction = _timeValue / _timeToCompleteQuestion;
            }
            else
            {
                IsAnsweringQuestion = false;
                _timeValue = _timeToShowCorrectAnswer;
            }
        }
        else
        {
            if (_timeValue > 0)
            {
                FillFraction = _timeValue / _timeToShowCorrectAnswer;
            }
            else
            {
                IsAnsweringQuestion = true;
                _timeValue = _timeToCompleteQuestion;
                LoadNextQuestion = true;
            }
        }
    }
}
