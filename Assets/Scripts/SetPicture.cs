using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPicture : MonoBehaviour
{
    [SerializeField] GameObject _gameboard;
    void Start()
    {
        Texture2D _image = _gameboard.GetComponent<GameBoard15>().GetImage();
        Debug.Log(_image.name);
        Rect _rect = new Rect(0, 0, 1440, 1440);
        Sprite _sprite = Sprite.Create(_image, _rect, new Vector2(0.5f, 0.5f));
        GetComponent<SpriteRenderer>().sprite = _sprite;
    }

}
