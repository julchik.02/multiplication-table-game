using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrawSettings : MonoBehaviour
{
    [SerializeField] GameObject _drawingObj;
    [SerializeField] GameObject[] _buttons;
    [SerializeField] GameObject _image;
    [SerializeField] Slider _mainSlider;
    Drawing _drawing;

    private void Awake()
    {
        _drawing = _drawingObj.GetComponent<Drawing>();
    }

    public void Start()
    {
        _mainSlider.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
    }
    public void SetColorToYellow()
    {
        _drawing.BrushColor = Color.yellow;
        _image.transform.position = _buttons[0].transform.position;
    }
    public void SetColorToRed()
    {
        _drawing.BrushColor = Color.red;
        _image.transform.position = _buttons[1].transform.position;
    }
    public void SetColorToGreen()
    {
        _drawing.BrushColor = Color.green;
        _image.transform.position = _buttons[2].transform.position;
    }
    public void SetColorToViolet()
    {
        _drawing.BrushColor = new Color(0.28f, 0f, 0.91f);
        _image.transform.position = _buttons[3].transform.position;
    }
    public void ValueChangeCheck()
    {
        _drawing.brushSize = (int)_mainSlider.value;
    }
    public void SetColorToOrange()
    {
        _drawing.BrushColor = new Color(0.93f, 0.55f, 0.28f);
        _image.transform.position = _buttons[4].transform.position;
    }
    public void SetColorToBlack()
    {
        _drawing.BrushColor = Color.black;
        _image.transform.position = _buttons[5].transform.position;
    }
    public void SetColorToBlue()
    {
        _drawing.BrushColor = Color.blue;
        _image.transform.position = _buttons[6].transform.position;
    }
    public void SetColorToBPink()
    {
        _drawing.BrushColor = new Color(0.89f, 0.42f, 0.78f);
        _image.transform.position = _buttons[7].transform.position;
    }
}
