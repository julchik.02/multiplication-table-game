using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PuzzleMaker : MonoBehaviour
{

    [SerializeField] Texture2D[] _puzzleList;
    [SerializeField] GameObject _puzzle;
    [SerializeField] int _pixelPerInit = 100;
    [SerializeField] GameObject _fireworks;
    [SerializeField] Button _menuButton;

    int _rows = 0;
    int _columns = 0;
    int _xOffcet = 150;
    int _yOffcet = 100;
    private string _puzzleTag = "GameController";
    string _nameGameObject = "Puzzle";
    private int _puzzleCounter, _sortingOrder;
    private Transform _current;
    private Vector3 _offset;
    private bool _isWinner;
    int _puzzleInPlace = 0;
    PuzzleProperties _puzzleProperties;
    List<GameObject> _puzzles = new List<GameObject>();
    Texture2D _image;


    private void Awake()
    {
        _rows = Random.Range(2, 7);
        _columns = Random.Range(2, 7);
        _image = _puzzleList[Random.Range(0, _puzzleList.Length)];
        _menuButton.onClick.AddListener(LoadMainScene);
    }
    void Start()
    {

        GenerateOriginalImage();
        GeneratePuzzle();


    }
    public void IncreasePuzzleInPlace()
    {
        _puzzleInPlace++;
    }

    private void GenerateOriginalImage()
    {
        GameObject _puzzleOriginal = new GameObject();
        Vector3 _posStart = Camera.main.WorldToScreenPoint(new Vector3(0f, 0f, 0f));
        Vector3 _posEnd = Camera.main.WorldToScreenPoint(new Vector3(_image.width, _image.height, 10f));

        int _width = (int)(_posEnd.x - _posStart.x);
        int _height = (int)(_posEnd.y - _posStart.y);
        Sprite _imageOriginal = Sprite.Create(_image,
                            new Rect(0, 0, _image.width, _image.height),
                            new Vector2(0.5f, 0.5f));
        var _spriteRenderer = _puzzleOriginal.AddComponent<SpriteRenderer>();

        _puzzleOriginal.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(_width / 2 / _pixelPerInit + _xOffcet, _height / 2 / _pixelPerInit + _yOffcet, 10f));
        _spriteRenderer.sprite = _imageOriginal;
        _spriteRenderer.sortingOrder = -2;
        var _color = Color.white;
        _color.a = 0.4f;
        _spriteRenderer.color = _color;
    }

    void GeneratePuzzle()
    {
        int _puzzleNumber = 0;
        Vector3 _posStart = Camera.main.WorldToScreenPoint(new Vector3(0f, 0f, 0f));
        Vector3 _posEnd = Camera.main.WorldToScreenPoint(new Vector3(_image.width, _image.height, 10f));

        int _width = (int)(_posEnd.x - _posStart.x);
        int _height = (int)(_posEnd.y - _posStart.y);
        int _cellPosWidht = _width / _columns / _pixelPerInit;
        int _cellPosHeigth = _height / _rows / _pixelPerInit;
        int _widthLeft = _image.width % _columns;
        int _heightLeft = _image.height % _rows;
        int _cellWidth = (_image.width - _widthLeft) / _columns;
        int _cellHeight = (_image.height - _heightLeft) / _rows;

        for (int i = 0; i < _columns; i++)
        {
            for (int j = 0; j < _rows; j++)
            {

                Color[] _imageColor = _image.GetPixels(i * _cellWidth, j * _cellHeight, _cellWidth, _cellHeight);
                Texture2D _cutImage = new Texture2D(_cellWidth, _cellHeight, TextureFormat.ARGB32, false);
                _cutImage.SetPixels(0, 0, _cellWidth, _cellHeight, _imageColor);
                _cutImage.Apply();
                Rect _rect = new Rect(0, 0, _cellWidth, _cellHeight);

                Sprite _imageNew = Sprite.Create(_cutImage, _rect, new Vector2(0.5f, 0.5f));

                string _nameObject = _nameGameObject + _puzzleNumber.ToString();
                GameObject _newCuttedImage = (GameObject)Instantiate(_puzzle, Camera.main.ScreenToWorldPoint(new Vector3(_cellPosWidht / 2 + _xOffcet + i * _cellPosWidht,
                                                                                    _cellPosHeigth / 2 + _yOffcet + j * _cellPosHeigth, 10)),
                                                    Quaternion.identity, this.transform);


                _newCuttedImage.name = _nameObject;
                _puzzleProperties = _newCuttedImage.GetComponent<PuzzleProperties>();
                _puzzleProperties.GetComponent<SpriteRenderer>().sprite = _imageNew;
                Vector2 _cellSize = new Vector2(_cellWidth / _pixelPerInit, _cellHeight / _pixelPerInit);
                _newCuttedImage.GetComponent<BoxCollider2D>().size = _cellSize;
                _puzzleProperties.SetTargetPosition(Camera.main.ScreenToWorldPoint(new Vector3(_cellPosWidht / 2 + _xOffcet + i * _cellPosWidht,
                                                                                    _cellPosHeigth / 2 + _yOffcet + j * _cellPosHeigth, 10)));
                _puzzles.Add(_newCuttedImage);
                _newCuttedImage.tag = _puzzleTag;
                _puzzleNumber++;

            }
        }
        foreach (GameObject _puzzle in _puzzles)
        {
            _puzzle.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(40, 850), Random.Range(40, 400), 10));
        }

    }
    private void Update()
    {
        MovePuzzle();
        WinCheck();
        Win();
    }

    private void Win()
    {
        if (_isWinner)
        {
            _fireworks.SetActive(true);
            _menuButton.gameObject.SetActive(true);
        }
    }

    private void WinCheck()
    {
        if (_puzzleInPlace == _rows * _columns)
        {
            _isWinner = true;
        }
    }

    private void MovePuzzle()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GetPuzzle();
        }
        else if (Input.GetMouseButtonUp(0) && _current)
        {
            _current.GetComponent<SpriteRenderer>().sortingOrder = _sortingOrder;
            _current = null;


        }
        if (_current)
        {
            Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            _current.position = new Vector3(position.x, position.y, _current.position.z) + new Vector3(_offset.x, _offset.y, 0);
        }
    }

    void GetPuzzle()
    {
        RaycastHit2D[] _hit = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (_hit.Length > 0 && _hit[0].transform.tag == _puzzleTag)
        {
            _offset = _hit[0].transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition); ;
            _current = _hit[0].transform;
            _sortingOrder = _current.GetComponent<SpriteRenderer>().sortingOrder;
            _current.GetComponent<SpriteRenderer>().sortingOrder = _puzzleCounter + 1;
        }
    }

    public void LoadMainScene()
    {
        SceneManager.LoadScene(0);
    }

}