using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleProperties : MonoBehaviour
{
    PuzzleMaker _puzzleMaker;
    Vector3 _targetPosition = new Vector3(0f, 0f, 0f);
    float _distance = 0.5f;
    string _puzzletag = "Placed";
    public void SetTargetPosition(Vector3 value)
    {
        _targetPosition = value;
    }
    private void Awake()
    {
        _puzzleMaker = FindObjectOfType<PuzzleMaker>();
    }
    private void Update()
    {

        if (Input.GetMouseButtonUp(0))
        {
            if (Vector3.Distance(transform.position, _targetPosition) < _distance)
            {
                if (this.tag != _puzzletag)
                {
                    Debug.Log("Match");
                    this.tag = _puzzletag;
                    transform.position = _targetPosition;
                    GetComponent<SpriteRenderer>().sortingOrder = -1;
                    _puzzleMaker.IncreasePuzzleInPlace();
                }

            }
        }



    }
}
