using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreKeeper : MonoBehaviour
{
    int _correctAnswers = 0;

    public int GetCorrectAnswers()
    {
        return _correctAnswers;
    }
    public void IncrementCorrectAnswers()
    {
        _correctAnswers++;
    }
    public int CalculateScore()
    {
        return _correctAnswers * 10;

    }
}
