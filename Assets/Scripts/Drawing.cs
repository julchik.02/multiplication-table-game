using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drawing : MonoBehaviour
{
    [SerializeField] int _textureWidth = 1000;
    [SerializeField] int _textureHeight = 1000;
    [SerializeField] float _pixelPerUnit = 100;
    public Color BrushColor;
    public int brushSize = 5;
    SpriteRenderer _spriteRenderer;
    Sprite _canvasDraw;
    Texture2D _drawTexture;
    Rect _rect;
    Vector2 _currentPixel, _startPoint, _endPoint, _currentPixelMatch;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _drawTexture = new Texture2D(_textureWidth, _textureHeight, TextureFormat.RGB24, false);
        _rect = new Rect(0, 0, _textureWidth, _textureHeight);
        _canvasDraw = Sprite.Create(_drawTexture, _rect, new Vector2(0.5f, 0.5f), _pixelPerUnit, 0);
        _spriteRenderer.sprite = _canvasDraw;

    }
    private void Start()
    {
        _currentPixel = GetWorldCoordinate(Input.mousePosition);
        _currentPixelMatch = new Vector2((_currentPixel.x + 5) * _pixelPerUnit, (_currentPixel.y + 5) * _pixelPerUnit);
        _startPoint = _currentPixelMatch;
    }
    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            _currentPixel = GetWorldCoordinate(Input.mousePosition);
            _currentPixelMatch = new Vector2((_currentPixel.x + 5) * _pixelPerUnit, (_currentPixel.y + 5) * _pixelPerUnit);
            if (_currentPixelMatch.x > 0 && _currentPixelMatch.x < _textureWidth && _currentPixelMatch.y > 0
                                                                && _currentPixelMatch.y < _textureHeight)
            {
                _endPoint = _currentPixelMatch;
                float _distance = Vector2.Distance(_startPoint, _endPoint);
                float _lerpSteps = 0.05f;
                var _pixels = _drawTexture.GetPixels32();

                for (float lerp = 0; lerp <= 1; lerp += _lerpSteps)
                {
                    _currentPixelMatch = Vector2.Lerp(_startPoint, _endPoint, lerp);
                    Drawer(_currentPixelMatch, ref _pixels);
                }
                _drawTexture.SetPixels32(_pixels);
                _drawTexture.Apply();
                _startPoint = _currentPixelMatch;
            }


        }
        _currentPixel = GetWorldCoordinate(Input.mousePosition);

        _currentPixelMatch = new Vector2((_currentPixel.x + 5) * _pixelPerUnit, (_currentPixel.y + 5) * _pixelPerUnit);
        _startPoint = _currentPixelMatch;


    }

    void Drawer(Vector2 currentPixel, ref Color32[] pixels)
    {

        for (int i = (int)currentPixel.x - brushSize; i <= (int)currentPixel.x + brushSize; i++)
        {
            for (int j = (int)currentPixel.y - brushSize; j <= (int)currentPixel.y + brushSize; j++)
            {
                pixels[j * _textureWidth + i] = BrushColor;
            }
        }
    }
    Vector2 GetWorldCoordinate(Vector3 _mousePosition)
    {
        Vector2 mousePoint = new Vector3(_mousePosition.x, _mousePosition.y, 0);
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }
    public void clearTexture()
    {
        _drawTexture = new Texture2D(_textureWidth, _textureHeight, TextureFormat.RGB24, false);
        _rect = new Rect(0, 0, _textureWidth, _textureHeight);
        _canvasDraw = Sprite.Create(_drawTexture, _rect, new Vector2(0.5f, 0.5f), _pixelPerUnit, 0);
        _spriteRenderer.sprite = _canvasDraw;
    }
}

