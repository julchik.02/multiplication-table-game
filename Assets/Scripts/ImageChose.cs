using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageChose : MonoBehaviour
{
    [SerializeField] Sprite[] _images;

    public void ImageChoose(int index)
    {
        gameObject.GetComponent<Image>().sprite = _images[index];

    }
}
