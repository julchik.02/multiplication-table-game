using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireworksManager : MonoBehaviour
{
    [SerializeField] GameObject[] _fireworks;
    int[] _fireworksIndexes;



    void Start()
    {
        _fireworksIndexes = new int[_fireworks.Length];

        for (int i = 0; i < _fireworksIndexes.Length; i++)
        {
            _fireworksIndexes[i] = i;
        }

        for (int index = 0; index < 4; index++)
        {
            for (int i = 0; i < _fireworksIndexes.Length; i++)
            {
                int j = Random.Range(0, _fireworksIndexes.Length);
                int tmp = _fireworksIndexes[j];
                _fireworksIndexes[j] = _fireworksIndexes[i];
                _fireworksIndexes[i] = tmp;
            }
        }
        StartCoroutine(StartingFireworks());


    }

    IEnumerator StartingFireworks()
    {


        for (int i = 0; i < _fireworks.Length; i++)
        {
            _fireworks[_fireworksIndexes[i]].GetComponent<ParticleSystem>().Play();
            _fireworks[_fireworksIndexes[i]].GetComponent<AudioSource>().Play();

            yield return new WaitForSeconds(Random.Range(0.2f, 0.4f));

        }



    }

}
