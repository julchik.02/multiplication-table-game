using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puzzle15 : MonoBehaviour
{
    private bool _wasMoved;
    private int _distanceBetweenCells = 4;

    public GameBoard15 gameBoard15;

    public int ID
    {
        get;
        set;
    }
    public Vector3 targetPosition
    {
        get;
        set;
    }
    public bool isOnTargetPosition
    {
        get;
        set;
    }
    private void Start()
    {
        gameBoard15 = FindObjectOfType<GameBoard15>();
        StartCoroutine(CheckTargetPosition());
    }

    private void OnMouseDown()
    {
        NeighborCheck();
    }

    private void NeighborCheck()
    {
        _wasMoved = false;

        CheckNeighborAndMove(Vector3.right * _distanceBetweenCells);
        CheckNeighborAndMove(Vector3.left * _distanceBetweenCells);
        CheckNeighborAndMove(Vector3.down * _distanceBetweenCells);
        CheckNeighborAndMove(Vector3.up * _distanceBetweenCells);
    }


    private void CheckNeighborAndMove(Vector3 direction)
    {
        if (_wasMoved)
            return;

        var ray = new Ray(transform.position, direction);
        if (!Physics.Raycast(ray, out _, 4f))
        {
            transform.Translate(direction);
            _wasMoved = true;
        }
    }
    IEnumerator CheckTargetPosition()
    {
        while (gameBoard15.IsWinner == false)
        {
            float distance = Vector3.Distance(targetPosition, transform.position);
            if (distance < 0.5f)
            {
                isOnTargetPosition = true;
            }
            else
            {
                isOnTargetPosition = false;
            }
            yield return new WaitForSeconds(5);
        }
    }


}