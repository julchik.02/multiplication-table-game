using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameBoard15 : MonoBehaviour
{
    [SerializeField] Texture2D[] _images;
    [SerializeField] GameObject _puzzle15;
    [SerializeField] GameObject _emptySpace;
    [SerializeField] GameObject _fireworks;
    public bool IsWinner = false;
    int _piecesInRowColumn = 4;
    int _puzzleNumber = 0;
    string _puzzleTag = "puzzlePieces";
    string _nameGameObject = "puzzlePieces";
    puzzle15 _puzzleProperties;
    int _pixelPerInit = 100;
    int _puzzleInPlace = 0;
    GameObject[] _cuttedImages = new GameObject[16];
    List<GameObject> _puzzles = new List<GameObject>();
    float _startPosX = -9;
    float _startPosY = -10;
    float _outY = 4;
    float _outX = 4;
    Vector3[,] _startPosition;
    int _emptySpaceIndex = 12;
    Texture2D _image;

    private void Awake()
    {
        _image = _images[Random.Range(0, _images.Length)];
    }

    void Start()
    {
        CuttPuzzles();
        StartCoroutine(CheckPuzzlePosition());
    }
    public Texture2D GetImage() { return _image; }

    void CuttPuzzles()
    {
        int cellWidth = _image.width / _piecesInRowColumn;
        int cellHeight = _image.height / _piecesInRowColumn;
        for (int i = 0; i < _piecesInRowColumn; i++)
        {
            for (int j = 0; j < _piecesInRowColumn; j++)
            {
                if ((j != 0) || (i != 3))
                {

                    Color[] _imageColor = _image.GetPixels(i * cellWidth, j * cellHeight, cellWidth, cellHeight);
                    Texture2D _cutImage = new Texture2D(cellWidth, cellHeight, TextureFormat.ARGB32, false);
                    _cutImage.SetPixels(0, 0, cellWidth, cellHeight, _imageColor);
                    _cutImage.Apply();
                    Rect _rect = new Rect(0, 0, cellWidth, cellHeight);
                    Sprite _imageNew = Sprite.Create(_cutImage, _rect, new Vector2(0.5f, 0.5f));
                    string _nameObject = _nameGameObject + _puzzleNumber.ToString();
                    GameObject _newCuttedImage = (GameObject)Instantiate(_puzzle15, new Vector3(-5 + i * (Mathf.CeilToInt(cellWidth / 100) + 1),
                                                                                                -6 + j * (Mathf.CeilToInt(cellHeight / 100) + 1), 0),
                                                                Quaternion.identity, this.transform);
                    _newCuttedImage.name = _nameObject;
                    _puzzleProperties = _newCuttedImage.GetComponent<puzzle15>();
                    _puzzleProperties.GetComponent<SpriteRenderer>().sprite = _imageNew;
                    Vector3 cellSize = new Vector3(cellWidth / _pixelPerInit, cellHeight / _pixelPerInit, 1);
                    _newCuttedImage.GetComponent<BoxCollider>().size = cellSize;
                    _puzzles.Add(_newCuttedImage);
                    _newCuttedImage.tag = _puzzleTag;
                    _puzzleProperties.ID = _puzzleNumber;
                    _cuttedImages[_puzzleNumber] = _newCuttedImage;
                    _puzzleProperties.targetPosition = _newCuttedImage.transform.position;
                    _puzzleNumber++;



                }
                else
                {
                    _emptySpace.GetComponent<EmptySpaceProperties>().TargetPosition = new Vector3(7, -6, 0);
                    _emptySpace.GetComponent<EmptySpaceProperties>().transform.position = new Vector3(7, -6, 0);
                    _cuttedImages[12] = _emptySpace;
                    _puzzleNumber++;
                }

            }
        }

        float posYreset = _startPosY;
        _startPosition = new Vector3[4, 4];
        for (int x = 0; x < 4; x++)
        {
            _startPosX += _outX;
            for (int y = 0; y < 4; y++)
            {
                _startPosY += _outY;
                _startPosition[x, y] = new Vector3(_startPosX, _startPosY, 0);
            }
            _startPosY = posYreset;
        }
        Randomize();
    }
    public void Randomize()
    {
        int[] _indexes = new int[16];
        for (int i = 0; i < _indexes.Length; i++)
        {
            _indexes[i] = i;
        }

        for (int i = 0; i < 100; i++)
        {
            int _index = Random.Range(0, 4);
            if (_index == 0 && (_emptySpaceIndex + 4) < 16)
            {
                Shuffle(4, ref _indexes);
            }
            if (_index == 1 && (_emptySpaceIndex - 4) > 0)
            {
                Shuffle(-4, ref _indexes);
            }
            if (_index == 2 && _emptySpaceIndex % 4 != 0)
            {
                Shuffle(-1, ref _indexes);
            }
            if (_index == 3 && _emptySpaceIndex % 4 != 3)
            {
                Shuffle(1, ref _indexes);
            }

        }
        string _array = string.Empty;

        for (int i = 0; i < _indexes.Length; i++)
        {
            _array += _indexes[i] + "-";
        }
        Debug.Log(_array);

        Vector3[] _positions = new Vector3[16];
        for (int i = 0; i < _cuttedImages.Length; i++)
        {
            _positions[i] = _cuttedImages[i].transform.position;
        }

        for (int i = 0; i < _cuttedImages.Length; i++)
        {
            _cuttedImages[_indexes[i]].transform.position = _positions[i];
        }
    }
    IEnumerator CheckPuzzlePosition()
    {

        while (_puzzleInPlace < 15)
        {
            _puzzleInPlace = 0;
            for (int x = 0; x < 16; x++)
            {
                if (_cuttedImages[x] != null && x != 12)
                {
                    if (_cuttedImages[x].GetComponent<puzzle15>().isOnTargetPosition)
                        _puzzleInPlace++;

                }
            }
            yield return new WaitForSeconds(5);
        }
        _fireworks.SetActive(true);

    }
    void Shuffle(int shift, ref int[] _indexes)
    {
        int _temp = _indexes[_emptySpaceIndex];
        _indexes[_emptySpaceIndex] = _indexes[_emptySpaceIndex + shift];
        _indexes[_emptySpaceIndex + shift] = _temp;
        _emptySpaceIndex = _indexes[_emptySpaceIndex];
    }

    public void LoadMainScene()
    {
        SceneManager.LoadScene(0);
    }
}
