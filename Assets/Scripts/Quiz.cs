using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Quiz : MonoBehaviour
{
    [Header("Questions")]
    [SerializeField] TextMeshProUGUI questionText;
    [SerializeField] List<QuestionSO> questions = new List<QuestionSO>();
    QuestionSO currentQuestion;
    [Header("Answer")]
    [SerializeField] GameObject[] answerButtons;
    int correctAnswerIndex;
    public bool hasAnsweredEarly;
    [Header("Button Colors")]
    [SerializeField] Sprite defaultAnswerSprite;
    [SerializeField] Sprite correctAnswerSprite;
    [Header("Timer")]
    [SerializeField] Image timerImage;

    [Header("Scooring")]
    [SerializeField] TextMeshProUGUI scoreText;
    ScoreKeeper scoreKeeper;
    Timer timer;
    [Header("Progress bar")]
    [SerializeField] Slider progressBar;
    public bool isComplete;
    int firstQuestionIndex = 0;
    int lastQuestionIndex = 0;
    public bool isLoaded = true;

    void Start()
    {
        timer = FindObjectOfType<Timer>();
        scoreKeeper = FindObjectOfType<ScoreKeeper>();
        progressBar.maxValue = 10;
        progressBar.value = 0;
    }
    private void Update()
    {

        timerImage.fillAmount = timer.FillFraction;
        if (timer.LoadNextQuestion)
        {
            hasAnsweredEarly = false;
            GetNextQuestion();
            timer.LoadNextQuestion = false;

        }
        else if (!hasAnsweredEarly && !timer.IsAnsweringQuestion)
        {

            DisplayAnswer(-1);
            SetButtonState(false);
        }
    }
    void GetNextQuestion()
    {
        if (scoreKeeper.CalculateScore() < 100)
        {
            SetButtonState(true);
            SetDefaultButtonSprite();
            GetRandomQuestion();
            DisplayQuestion();

        }
    }
    void GetRandomQuestion()
    {
        int index = Random.Range(firstQuestionIndex, lastQuestionIndex);
        currentQuestion = questions[index];
    }
    void DisplayQuestion()
    {
        questionText.text = currentQuestion.GetQuestion();

        for (int i = 0; i < answerButtons.Length; i++)
        {
            TextMeshProUGUI buttonText = answerButtons[i].GetComponentInChildren<TextMeshProUGUI>();
            buttonText.text = currentQuestion.GetAnswer(i);
        }
    }

    void SetButtonState(bool state)
    {
        for (int i = 0; i < answerButtons.Length; i++)
        {
            Button button = answerButtons[i].GetComponent<Button>();
            button.interactable = state;
        }
    }
    public void OnAnswerSelected(int index)
    {
        hasAnsweredEarly = true;
        DisplayAnswer(index);
        SetButtonState(false);
        timer.CancelTimer();
        scoreText.text = "Кількість балів: " + scoreKeeper.CalculateScore();
    }

    private void DisplayAnswer(int index)
    {
        if (currentQuestion == null)
        {
            return;
        }

        Image buttonImage;
        if (index == currentQuestion.GetCorrectAnswerIndex())
        {
            questionText.text = "Правильно!";
            buttonImage = answerButtons[index].GetComponent<Image>();
            buttonImage.sprite = correctAnswerSprite;
            scoreKeeper.IncrementCorrectAnswers();
            progressBar.value++;
            if (progressBar.value == progressBar.maxValue)
            {
                isComplete = true;
            }
        }
        else
        {
            correctAnswerIndex = currentQuestion.GetCorrectAnswerIndex();
            string correctAnswer = currentQuestion.GetAnswer(correctAnswerIndex);
            questionText.text = "Неправильно, вірна відповідь - " + correctAnswer;
            buttonImage = answerButtons[correctAnswerIndex].GetComponent<Image>();
            buttonImage.sprite = correctAnswerSprite;
        }
    }

    void SetDefaultButtonSprite()
    {
        Image buttonImage;
        for (int i = 0; i < answerButtons.Length; i++)
        {
            buttonImage = answerButtons[i].GetComponent<Image>();
            buttonImage.sprite = defaultAnswerSprite;
        }

    }
    public void MultiplicationTable2()
    {
        firstQuestionIndex = 0;
        lastQuestionIndex = 9;
        isLoaded = true;

    }
    public void MultiplicationTable3()
    {
        firstQuestionIndex = 10;
        lastQuestionIndex = 19;
        isLoaded = true;
    }
    public void MultiplicationTable4()
    {
        firstQuestionIndex = 20;
        lastQuestionIndex = 29;
        isLoaded = true;
    }
    public void MultiplicationTable5()
    {
        firstQuestionIndex = 30;
        lastQuestionIndex = 39;
        isLoaded = true;
    }
    public void MultiplicationTable6()
    {
        firstQuestionIndex = 40;
        lastQuestionIndex = 49;
        isLoaded = true;
    }
    public void MultiplicationTable7()
    {
        firstQuestionIndex = 50;
        lastQuestionIndex = 59;
        isLoaded = true;
    }
    public void MultiplicationTable8()
    {
        firstQuestionIndex = 60;
        lastQuestionIndex = 69;
        isLoaded = true;
    }
    public void MultiplicationTable9()
    {
        firstQuestionIndex = 70;
        lastQuestionIndex = 79;
        isLoaded = true;
    }
}
